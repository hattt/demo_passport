// load all the things we need
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var q = require('q');

require('mongoose').Promise = q.Promise;
// load up the user model
var User = require('../app/models/user');

var configAuth = require('./auth');

// expose this function to our app using module.exports
module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function(req, email, password, done) {
            debugger;
            var user = req.user; //session user

            //facebook user
            User.findOne({
                'local.email': email
            }).then((xuser) => {

                //NOT YET LOGIN
                if (!req.user) {
                    //xuser exist --> fail create
                    if (xuser) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else { // create the user

                        var newUser = new User();

                        // set the user's local credentials
                        newUser.local = {
                            email: email,
                            password: newUser.generateHash(password),
                        };
                        newUser.save(function(err) {
                            if (err)
                                return done(err);
                            return done(null, newUser);
                        });
                    }
                } else { //LOGGED IN (--> CONNECT/AUTHORIZE)
                    if (xuser) //xuser exist -> fail create & connect
                        return done(null, false, req.flash('message', 'Local Account' + email + ' exist!'));


                    // update the session user's local credentials
                    user.local = {
                        email: email,
                        password: user.generateHash(password),
                    };
                    user.save(function(err) {
                        if (err)
                            return done(err);
                        return done(null, user);
                    });
                }
            }).catch((err) => {
                done(err);
            })


        }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function(req, email, password, done) { // callback with email and password from our form

            User.findOne({
                'local.email': email
            }).then((user) => {
                // user not exist
                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

                //password wrong
                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

                //right -> return user
                return done(null, user);
            }).catch((err) => {
                done(err);
            });

        }));


    // =========================================================================
    // FACEBOOK ==================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

            // pull in our app id and secret from our auth.js file
            clientID: configAuth.facebookAuth.clientID,
            clientSecret: configAuth.facebookAuth.clientSecret,
            callbackURL: configAuth.facebookAuth.callbackURL,
            passReqToCallback: true, // allows us to pass in the req from our route (lets us check if a user is logged in or not)

            profileFields: ['id', 'email', 'name'],

        },

        // facebook will send back the token and profile
        function(req, token, refreshToken, profile, done) {

            debugger;
            // make the code asynchronous: won't process until we have all our data back from provider
            process.nextTick(function() {
                var user = req.user; //session user

                //facebook user
                User.findOne({
                    'facebook.id': profile.id
                }).then((xuser) => {

                    //NOT YET LOGIN
                    if (!user) {

                        if (xuser)
                            return done(null, xuser); //xuser exist, return  xuser
                        else { //xuser not exist -> create new

                            var newUser = new User();

                            newUser.facebook = {
                                id: profile.id,
                                token: token,
                                name: profile.name.givenName + ' ' + profile.name.familyName,
                                emails: profile.emails ? profile.emails[0].value : undefined
                            };

                            newUser.save(function(err) {
                                if (err) return done(err);
                                return done(null, newUser);
                            });
                        }
                    } else { //LOGGED IN ( --> CONNECT/AUTHORIZE)

                        //xuser exist -> false connect
                        if (xuser) {
                            return done(null, false, req.flash('message', 'Facebook ' + profile.name.givenName + ' ' + profile.name.familyName + ' is already connect to other account.'));
                        }

                        // update the session user's facebook credentials
                        user.facebook = {
                            id: profile.id,
                            token: token,
                            name: profile.name.givenName + ' ' + profile.name.familyName,
                            emails: profile.emails ? profile.emails[0].value : undefined
                        };
                        user.save(function(err) {
                            if (err) return done(err);
                            return done(null, user);
                        });
                    }
                }).catch((err) => {
                    done(err);
                });
            });

        }));

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({

            clientID: configAuth.googleAuth.clientID,
            clientSecret: configAuth.googleAuth.clientSecret,
            callbackURL: configAuth.googleAuth.callbackURL,
            passReqToCallback: true, // allows us to pass in the req from our route (lets us check if a user is logged in or not)

        },
        function(req, token, refreshToken, profile, done) {

            // make the code asynchronous: won't process until we have all our data back from provider
            process.nextTick(function() {
                var user = req.user; //session user

                //google user
                User.findOne({
                    'google.id': profile.id
                }).then((xuser) => {

                    //NOT YET LOGIN
                    if (!user) {

                        if (xuser)
                            return done(null, xuser); //xuser exist, return  xuser
                        else { //xuser not exist -> create new

                            var newUser = new User();

                            newUser.google = {
                                id: profile.id,
                                token: token,
                                name: profile.displayName,
                                emails: profile.emails ? profile.emails[0].value : undefined
                            };

                            newUser.save(function(err) {
                                if (err) return done(err);
                                return done(null, newUser);
                            });
                        }
                    } else { //LOGGED IN ( --> CONNECT/AUTHORIZE)

                        //xuser exist -> false connect
                        if (xuser) {
                            return done(null, false, req.flash('message', 'Account ' + profile.emails[0].value + ' is already connect to other account.'));
                        }

                        // update the session users google credentials
                        user.google = {
                            id: profile.id,
                            token: token,
                            name: profile.displayName,
                            emails: profile.emails ? profile.emails[0].value : undefined
                        };
                        user.save(function(err) {
                            if (err) return done(err);
                            return done(null, user);
                        });
                    }
                }).catch((err) => {
                    done(err);
                });
            });

        }));

    // =========================================================================
    // TWITTER ==================================================================
    // =========================================================================
    passport.use(new TwitterStrategy({

            consumerKey: configAuth.twitterAuth.consumerKey,
            consumerSecret: configAuth.twitterAuth.consumerSecret,
            callbackURL: configAuth.twitterAuth.callbackURL,
            passReqToCallback: true, // allows us to pass in the req from our route (lets us check if a user is logged in or not)

        },
        function(req, token, refreshToken, profile, done) {

            // make the code asynchronous: won't process until we have all our data back from provider
            process.nextTick(function() {
                var user = req.user; //session user

                //google user
                User.findOne({
                    'twitter.id': profile.id
                }).then((xuser) => {

                    //NOT YET LOGIN
                    if (!user) {

                        if (xuser)
                            return done(null, xuser); //xuser exist, return  xuser
                        else { //xuser not exist -> create new

                            var newUser = new User();

                            newUser.twitter = {
                                id: profile.id,
                                token: token,
                                username: profile.username,
                                displayName: profile.displayName
                            };

                            newUser.save(function(err) {
                                if (err) return done(err);
                                return done(null, newUser);
                            });
                        }
                    } else { //LOGGED IN ( --> CONNECT/AUTHORIZE)

                        //xuser exist -> false connect
                        if (xuser) {
                            return done(null, false, req.flash('message', 'Account ' + profile.username + ' is already connect to other account.'));
                        }

                        // update the session users twitter credentials
                        user.twitter = {
                            id: profile.id,
                            token: token,
                            username: profile.username,
                            displayName: profile.displayName
                        };
                        user.save(function(err) {
                            if (err) return done(err);
                            return done(null, user);
                        });
                    }
                }).catch((err) => {
                    done(err);
                });
            });

        }));

};