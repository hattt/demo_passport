module.exports = function(app, passport) {

    app.get('/', function(req, res) {
        res.render('index.ejs');
    });


    app.get('/login', function(req, res) {

        res.render('login.ejs', {
            message: req.flash('loginMessage')
        });
    });

    app.post('/login', (req, res, next) => {
        passport.authenticate('local-login', function(err, user, info) {
            debugger;
            if (err) {
                return next(err);
            }
            if (!user) {
                return res.redirect('/login');
            }
            req.logIn(user, function(err) {
                if (err) {
                    return next(err);
                }
                return res.redirect('/profile');
            });
        })(req, res, next);
    });

    app.get('/signup', function(req, res) {
        res.render('signup.ejs', {
            message: req.flash('signupMessage')
        });
    });
    app.post('/signup', (req, res, next) => {
        passport.authenticate('local-signup', function(err, user, info) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return res.redirect('/signup');
            }
            req.logIn(user, function(err) {
                if (err) {
                    return next(err);
                }
                return res.redirect('/profile');
            });
        })(req, res, next);
    });

    app.get('/profile', isLoggedIn, function(req, res) {

        res.render('profile.ejs', {
            user: req.user,
            message: req.flash('message')
        });
    });

    // =====================================
    // FACEBOOK ROUTES =====================
    // =====================================
    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', {
        callbackURL: '/auth/facebook/callback',
        scope: 'email'
    }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));


    // =====================================
    // GOOGLE ROUTES =======================
    // =====================================
    // send to google to do the authentication
    // profile gets us their basic information including their name
    // email gets their emails
    app.get('/auth/google', passport.authenticate('google', {
        callbackURL: '/auth/google/callback',
        scope: ['profile', 'email']
    }));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));

    // =====================================
    // TWITTER ROUTES =====================
    // =====================================
    app.get('/auth/twitter', passport.authenticate('twitter', {
        callbackURL: '/auth/twitter/callback'
    }));

    // handle the callback after twitter has authenticated the user
    app.get('/auth/twitter/callback',
        passport.authenticate('twitter', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));

    // =============================================================================
    // AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
    // =============================================================================

    // locally --------------------------------
    // app.get('/connect/local', function(req, res) {
    //     res.render('connect-local.ejs', {
    //         message: req.flash('message')
    //     });
    // });
    // app.post('/connect/local', passport.authenticate('local-signup', {
    //     successRedirect: '/profile',
    //     failureRedirect: '/connect/local',
    //     failureFlash: true // allow flash messages
    // }));

    // facebook -------------------------------

    // send to facebook to do the authentication
    app.get('/connect/facebook', passport.authorize('facebook', {
        callbackURL: '/connect/facebook/callback',
        scope: 'email'
    }));

    // handle the callback after facebook has authorized the user
    app.get('/connect/facebook/callback',
        passport.authorize('facebook', {
            callbackURL: '/connect/facebook/callback',
            failureRedirect: '/profile',
            successRedirect: '/profile'
        }), (req, res, next) => {
            debugger;
            return res.redirect('/profile');
        });

    // twitter --------------------------------

    // send to twitter to do the authentication
    app.get('/connect/twitter', passport.authorize('twitter', {
        callbackURL: '/connect/twitter/callback',
    }));

    // handle the callback after twitter has authorized the user
    app.get('/connect/twitter/callback',
        passport.authorize('twitter', {
            callbackURL: '/connect/twitter/callback',
            successRedirect: '/profile',
            failureRedirect: '/profile'
        }), (req, res, next) => {
            debugger;
            return res.redirect('/profile');
        });

    // google ---------------------------------

    // send to google to do the authentication
    app.get('/connect/google', passport.authorize('google', {
        callbackURL: '/connect/google/callback',
        scope: ['profile', 'email']
    }));

    // the callback after google has authorized the user
    app.get('/connect/google/callback',
        passport.authorize('google', {
            callbackURL: '/connect/google/callback',
            failureRedirect: '/profile',
            successRedirect: '/profile'
        }), (req, res, next) => {
            debugger;
            return res.redirect('/profile');
        });

    // =============================================================================
    // UNLINK ACCOUNTS =============================================================
    // =============================================================================
    // used to unlink accounts. for social accounts, just remove the token
    // for local account, remove email and password
    // user account will stay active in case they want to reconnect in the future


    // facebook -------------------------------
    app.get('/unlink/facebook', function(req, res) {
        var user = req.user;

        user.facebook = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });

    // twitter --------------------------------
    app.get('/unlink/twitter', function(req, res) {
        var user = req.user;

        user.twitter = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });

    // google ---------------------------------
    app.get('/unlink/google', function(req, res) {
        var user = req.user;

        user.google = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    app.use((err, req, res, next) => {
        console.log('HERE');
        debugger;
        console.log(err);
        res.status(500).send(err);
    });
    app.use((req, res, next) => {
        debugger;
        res.status(404).send('NOT FOUND');
    });
};


// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}