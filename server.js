// set up ======================================================================
var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var q = require('q');

var configDB = require('./config/database.js');

// configuration ===============================================================
mongoose.Promise = q.Promise;

mongoose.connect(configDB.url, {
		useMongoClient: true,
		reconnectTries: 30,
		reconnectInterval: 1000,
		keepAlive: 1,
		connectTimeoutMS: 30000
	})
	.then(() => {
		console.log('SUCCESS MGDB');
	})
	.catch((err) => {
		console.log(err);
	});

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev'));
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));


app.set('view engine', 'ejs');

// required for passport
app.use(session({
	key: 'session_cookie_name',
	secret: 'session_cookie_secret',
	resave: false,
	saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
app.listen(port);
console.log('Listening on port ' + port);